package com.astro.blog.application.configuration.errors;

public class TechnicalException extends RuntimeException {

    private static final long serialVersionUID = 1871556601171218022L;

    public TechnicalException() {
        this("Technical exception has occurred");
    }

    public TechnicalException(String message) {
        this(message, (Throwable) null);
    }

    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

}
