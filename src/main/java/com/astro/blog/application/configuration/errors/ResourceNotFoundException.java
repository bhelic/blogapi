package com.astro.blog.application.configuration.errors;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -1361801433330793685L;

    public ResourceNotFoundException() {
        this("EntityRepresentationModel not found");
    }

    public ResourceNotFoundException(String message) {
        this(message, (Throwable) null);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
