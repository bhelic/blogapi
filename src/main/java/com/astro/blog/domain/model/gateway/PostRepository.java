package com.astro.blog.domain.model.gateway;

import com.astro.blog.domain.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PostRepository {

    Page<Post> findPosts(Pageable page);

    Post findPostById(Long id);

    List<Post> findPostsByTitle(String title);

    List<Post> saveAll(List<Post> posts);

}
