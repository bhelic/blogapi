package com.astro.blog.domain.model.gateway.port;

import com.astro.blog.domain.usecase.dto.PostResponseDto;

public interface PostPort {

    PostResponseDto[] getPosts();

}
