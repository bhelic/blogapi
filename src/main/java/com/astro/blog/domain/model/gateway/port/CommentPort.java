package com.astro.blog.domain.model.gateway.port;

import com.astro.blog.domain.usecase.dto.CommentResponseDto;

public interface CommentPort {

    CommentResponseDto[] getComments(Long id);

}
