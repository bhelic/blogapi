package com.astro.blog.domain.model.gateway;

import com.astro.blog.domain.model.Comment;

import java.util.List;

public interface CommentRepository {

    List<Comment> findCommentsByPostId(Long id);
    List<Comment> saveAll(List<Comment> comments);

}
