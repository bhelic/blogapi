package com.astro.blog.domain.usecase;

import com.astro.blog.domain.model.Comment;
import com.astro.blog.domain.model.Post;
import com.astro.blog.domain.model.gateway.CommentRepository;
import com.astro.blog.domain.model.gateway.PostRepository;
import com.astro.blog.domain.model.gateway.port.CommentPort;
import com.astro.blog.domain.model.gateway.port.PostPort;
import com.astro.blog.domain.usecase.dto.CommentResponseDto;
import com.astro.blog.domain.usecase.dto.PostResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class InitDataUseCase {

    private final PostPort postPort;
    private final CommentPort commentPort;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final ObjectMapper objectMapper;

    public InitDataUseCase(PostPort postPort,
                           CommentPort commentPort,
                           PostRepository postRepository,
                           CommentRepository commentRepository,
                           ObjectMapper objectMapper) {
        this.postPort = postPort;
        this.commentPort = commentPort;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    @Transactional
    public List<Post> initData() {
        log.info("Initializing data of posts and comments");
        List<Post> posts = savePosts();
        saveComments(posts);
        log.info("Data of posts and comments ready");
        return posts;
    }

    private List<Post> savePosts() {
        log.info("Executing method savePosts");
        PostResponseDto[] posts = postPort.getPosts();

        List<Post> postList = Arrays.stream(posts)
                .map(postDto -> objectMapper.convertValue(postDto, Post.class))
                .collect(Collectors.toList());

        return postRepository.saveAll(postList);
    }

    private void saveComments(List<Post> posts) {
        log.info("Executing method saveComments");
        posts.stream().forEach(post -> {
            CommentResponseDto[] comments = commentPort.getComments(post.getId());

            List<Comment> commentList = Arrays.stream(comments)
                    .map(commentDto -> {
                        Comment comment = objectMapper.convertValue(commentDto, Comment.class);
                        comment.setPost(post);
                        return comment;
                    }).collect(Collectors.toList());

            commentRepository.saveAll(commentList);
        });
    }

}
