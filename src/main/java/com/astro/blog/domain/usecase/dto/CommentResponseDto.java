package com.astro.blog.domain.usecase.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentResponseDto {
    private Long id;
    private Long postId;
    private String name;
    private String email;
    private String body;
}
