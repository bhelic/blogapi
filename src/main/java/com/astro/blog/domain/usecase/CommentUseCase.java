package com.astro.blog.domain.usecase;

import com.astro.blog.domain.model.gateway.CommentRepository;
import com.astro.blog.domain.usecase.dto.CommentResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommentUseCase {

    private final CommentRepository commentRepository;
    private final ObjectMapper objectMapper;

    public CommentUseCase(CommentRepository commentRepository,
                          ObjectMapper objectMapper) {
        this.commentRepository = commentRepository;
        this.objectMapper = objectMapper;
    }

    public List<CommentResponseDto> getCommentsByPostId(Long id) {
        log.info("Executing getCommentsByPostId with post id: " + id);
        return commentRepository.findCommentsByPostId(id)
                .stream()
                .map(comment -> {
                    CommentResponseDto commentResponseDto = objectMapper.convertValue(comment, CommentResponseDto.class);
                    commentResponseDto.setPostId(comment.getPost().getId());
                    return commentResponseDto;
                }).collect(Collectors.toList());
    }

}
