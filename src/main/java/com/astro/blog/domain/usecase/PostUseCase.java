package com.astro.blog.domain.usecase;

import com.astro.blog.domain.model.Post;
import com.astro.blog.domain.model.gateway.PostRepository;
import com.astro.blog.domain.usecase.dto.PostResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PostUseCase {

    private final PostRepository postRepository;
    private final ObjectMapper objectMapper;

    public PostUseCase(PostRepository postRepository,
                       ObjectMapper objectMapper) {
        this.postRepository = postRepository;
        this.objectMapper = objectMapper;
    }

    public Page<PostResponseDto> getPosts(Pageable page) {
        log.info("Executing getPostsByPage: " + page);
        return postRepository.findPosts(page)
                .map(post -> objectMapper.convertValue(post, PostResponseDto.class));
    }

    public PostResponseDto getPostById(Long id) {
        log.info("Executing getPostById with id: " + id);
        Post post = postRepository.findPostById(id);
        return objectMapper.convertValue(post, PostResponseDto.class);
    }

    public List<PostResponseDto> getPostsByTitle(String title) {
        log.info("Executing getPostsByTitle with title: " + title);
        return postRepository.findPostsByTitle(title)
                .stream()
                .map(post -> objectMapper.convertValue(post, PostResponseDto.class))
                .collect(Collectors.toList());
    }

}
