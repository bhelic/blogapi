package com.astro.blog.domain.usecase.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostResponseDto {
    private Long id;
    private Long userId;
    private String title;
    private String body;
}
