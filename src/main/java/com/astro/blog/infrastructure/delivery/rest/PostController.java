package com.astro.blog.infrastructure.delivery.rest;

import com.astro.blog.domain.model.Post;
import com.astro.blog.domain.usecase.CommentUseCase;
import com.astro.blog.domain.usecase.PostUseCase;
import com.astro.blog.domain.usecase.dto.CommentResponseDto;
import com.astro.blog.domain.usecase.dto.PostResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springdoc.api.ErrorMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {

    private final PostUseCase postUseCase;
    private final CommentUseCase commentUseCase;

    public PostController(PostUseCase postUseCase,
                          CommentUseCase commentUseCase) {
        this.postUseCase = postUseCase;
        this.commentUseCase = commentUseCase;
    }

    @GetMapping
    @Operation(summary = "Get posts page")
    public ResponseEntity<Page<PostResponseDto>> getPosts(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.status(HttpStatus.OK).body(postUseCase.getPosts(PageRequest.of(page, size)));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get post by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Post not found",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ErrorMessage.class))})
    })
    public ResponseEntity<PostResponseDto> getPostById(@PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(postUseCase.getPostById(id));
    }

    @GetMapping("/{post-id}/comments")
    @Operation(summary = "Get comments by post id")
    public ResponseEntity<List<CommentResponseDto>> getCommentsByPostsId(@PathVariable("post-id") Long postId) {
        return ResponseEntity.status(HttpStatus.OK).body(commentUseCase.getCommentsByPostId(postId));
    }

    @GetMapping("/title")
    @Operation(summary = "Get posts by title")
    public ResponseEntity<List<PostResponseDto>> getPostsByTitle(@RequestParam("title") String title) {
        return ResponseEntity.status(HttpStatus.OK).body(postUseCase.getPostsByTitle(title));
    }

}
