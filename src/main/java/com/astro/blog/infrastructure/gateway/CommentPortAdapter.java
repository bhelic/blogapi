package com.astro.blog.infrastructure.gateway;

import com.astro.blog.application.configuration.errors.TechnicalException;
import com.astro.blog.domain.model.gateway.port.CommentPort;
import com.astro.blog.domain.usecase.dto.CommentResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

@Service
@Slf4j
public class CommentPortAdapter implements CommentPort {

    private final WebClient webClient;
    private final String jsonPlaceholderUrl;

    public CommentPortAdapter(WebClient.Builder webClientBuilder,
                              @Value("${app.gateway.jsonplaceholder.url}") String jsonPlaceholderUrl) {
        this.webClient = webClientBuilder.baseUrl(jsonPlaceholderUrl).build();
        this.jsonPlaceholderUrl = jsonPlaceholderUrl;
    }

    @Override
    public CommentResponseDto[] getComments(Long id) {
        log.info("Getting comments for post with id: " + id);
        try {
            return webClient.get()
                    .uri(String.format("/posts/%s/comments", id))
                    .retrieve()
                    .bodyToMono(CommentResponseDto[].class)
                    .block();

        } catch (WebClientException e) {
            log.error("Error in CommentPortAdapter - method: getComments", e);
            throw new TechnicalException("Error when trying get comments from " + jsonPlaceholderUrl, e);
        }
    }

}
