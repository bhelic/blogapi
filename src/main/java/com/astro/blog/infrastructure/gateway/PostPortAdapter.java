package com.astro.blog.infrastructure.gateway;

import com.astro.blog.application.configuration.errors.TechnicalException;
import com.astro.blog.domain.model.gateway.port.PostPort;
import com.astro.blog.domain.usecase.dto.PostResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

@Service
@Slf4j
public class PostPortAdapter implements PostPort {

    private final WebClient webClient;
    private final String jsonPlaceholderUrl;

    public PostPortAdapter(WebClient.Builder webClientBuilder,
                           @Value("${app.gateway.jsonplaceholder.url}") String jsonPlaceholderUrl) {
        this.webClient = webClientBuilder.baseUrl(jsonPlaceholderUrl).build();
        this.jsonPlaceholderUrl = jsonPlaceholderUrl;
    }

    @Override
    public PostResponseDto[] getPosts() {
        log.info("Getting posts");
        try {
            return webClient.get()
                    .uri("/posts")
                    .retrieve()
                    .bodyToMono(PostResponseDto[].class)
                    .block();

        } catch (WebClientException e) {
            log.error("Error in PostPortAdapter - method: getPosts", e);
            throw new TechnicalException("Error when trying get posts from " + jsonPlaceholderUrl, e);
        }
    }

}
