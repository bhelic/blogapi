package com.astro.blog.infrastructure.repository.jpa;

import com.astro.blog.domain.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentJpaRepository extends JpaRepository<Comment, Long> {

    List<Comment> findCommentsByPostId(Long id);

}
