package com.astro.blog.infrastructure.repository;

import com.astro.blog.application.configuration.errors.TechnicalException;
import com.astro.blog.domain.model.Comment;
import com.astro.blog.domain.model.gateway.CommentRepository;
import com.astro.blog.infrastructure.repository.jpa.CommentJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.persistence.PersistenceException;
import java.util.List;

@Component
@Slf4j
public class CommentRepositoryAdapter implements CommentRepository {

    private final CommentJpaRepository commentJpaRepository;

    public CommentRepositoryAdapter(CommentJpaRepository commentJpaRepository) {
        this.commentJpaRepository = commentJpaRepository;
    }

    @Override
    public List<Comment> findCommentsByPostId(Long id) {
        log.info("Executing findCommentsByPostId with post id: " + id);
        try {
            return commentJpaRepository.findCommentsByPostId(id);

        } catch (PersistenceException e) {
            log.error("Error in CommentRepositoryAdapter - method: findCommentsByPostId", e);
            throw new TechnicalException("Error when trying find comments by post id", e);
        }
    }

    @Override
    public List<Comment> saveAll(List<Comment> comments) {
        log.info("Saving all comments");
        try {
            return commentJpaRepository.saveAll(comments);

        } catch (PersistenceException e) {
            log.error("Error in CommentRepositoryAdapter - method: saveAll", e);
            throw new TechnicalException("Error when trying save comments", e);
        }
    }

}
