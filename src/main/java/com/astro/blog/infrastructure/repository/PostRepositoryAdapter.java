package com.astro.blog.infrastructure.repository;

import com.astro.blog.application.configuration.errors.ResourceNotFoundException;
import com.astro.blog.application.configuration.errors.TechnicalException;
import com.astro.blog.domain.model.Post;
import com.astro.blog.domain.model.gateway.PostRepository;
import com.astro.blog.infrastructure.repository.jpa.PostJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class PostRepositoryAdapter implements PostRepository {

    private final PostJpaRepository postJpaRepository;

    public PostRepositoryAdapter(PostJpaRepository postJpaRepository) {
        this.postJpaRepository = postJpaRepository;
    }

    @Override
    public Page<Post> findPosts(Pageable page) {
        log.info("Executing findPosts: " + page.toString());
        try {
            return postJpaRepository.findAll(page);

        } catch (PersistenceException e) {
            log.error("Error in PostRepositoryAdapter - method: findPosts", e);
            throw new TechnicalException("Error when trying get posts page", e);
        }
    }

    @Override
    public Post findPostById(Long id) {
        log.info("Executing findPostById with id: " + id);
        try {
            Optional<Post> postOptional = postJpaRepository.findById(id);

            if (postOptional.isPresent()) {
                return postOptional.get();
            } else {
                throw new ResourceNotFoundException("Post id not found");
            }
        } catch (PersistenceException e) {
            log.error("Error in PostRepositoryAdapter - method: findPostById", e);
            throw new TechnicalException("Error when trying get post by id", e);
        }
    }

    @Override
    public List<Post> findPostsByTitle(String title) {
        log.info("Executing getPostsByTitle with title: " + title);
        try {
            return postJpaRepository.findPostsByTitleContains(title);

        } catch (PersistenceException e) {
            log.error("Error in PostRepositoryAdapter - method: findPostsByTitle", e);
            throw new TechnicalException("Error when trying get posts by title", e);
        }
    }

    @Override
    public List<Post> saveAll(List<Post> posts) {
        log.info("Saving all posts");
        try {
            return postJpaRepository.saveAll(posts);

        } catch (PersistenceException e) {
            log.error("Error in PostRepositoryAdapter - method: saveAll", e);
            throw new TechnicalException("Error when trying save posts", e);
        }
    }

}
