package com.astro.blog.infrastructure.repository.jpa;

import com.astro.blog.domain.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostJpaRepository extends JpaRepository<Post, Long> {

    List<Post> findPostsByTitleContains(String title);

}
