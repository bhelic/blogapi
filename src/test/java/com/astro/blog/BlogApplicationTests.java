package com.astro.blog;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.astro.blog"})
class BlogApplicationTests {

    @Test
    void contextLoads() {
    }

}
