package com.astro.blog.infrastructure.repository;

import com.astro.blog.domain.model.Comment;
import com.astro.blog.infrastructure.repository.jpa.CommentJpaRepository;
import com.astro.blog.infrastructure.repository.jpa.PostJpaRepository;
import mocks.CommentMock;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

@SpringBootTest
public class CommentRepositoryAdapterTest {

    @Autowired
    private CommentRepositoryAdapter commentRepositoryAdapter;

    @MockBean
    private CommentJpaRepository commentJpaRepository;

    @MockBean
    private PostJpaRepository postJpaRepository;

    @Test
    public void Find_comments_by_post_id_return_ok() {
        //Given
        Long anyPostId = 1L;
        List<Comment> commentListMock = List.of(CommentMock.getComment(), CommentMock.getComment());
        Mockito.when(commentJpaRepository.findCommentsByPostId(eq(anyPostId))).thenReturn(commentListMock);
        //When
        List<Comment> commentListResult = commentRepositoryAdapter.findCommentsByPostId(anyPostId);
        //Then
        assertEquals(commentListMock.get(0).getId(), commentListResult.get(0).getId());
        Mockito.verify(this.commentJpaRepository, Mockito.times(1))
                .findCommentsByPostId(eq(anyPostId));
    }

    @Test
    public void Save_comments_return_ok() {
        //Given
        List<Comment> commentListMock = List.of(CommentMock.getComment(), CommentMock.getComment());
        Mockito.when(commentJpaRepository.saveAll(eq(commentListMock))).thenReturn(commentListMock);
        //When
        List<Comment> commentListResult = commentRepositoryAdapter.saveAll(commentListMock);
        //Then
        assertEquals(commentListMock.get(0).getId(), commentListResult.get(0).getId());
        assertEquals(commentListMock.get(0).getName(), commentListResult.get(0).getName());
        assertEquals(commentListMock.get(0).getEmail(), commentListResult.get(0).getEmail());
        assertEquals(commentListMock.get(0).getBody(), commentListResult.get(0).getBody());
        Mockito.verify(this.commentJpaRepository, Mockito.times(1)).saveAll(eq(commentListMock));
    }

}
