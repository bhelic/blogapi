package com.astro.blog.infrastructure.repository;

import com.astro.blog.application.configuration.errors.ResourceNotFoundException;
import com.astro.blog.domain.model.Post;
import com.astro.blog.infrastructure.repository.jpa.PostJpaRepository;
import mocks.PostMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;

@SpringBootTest
public class PostRepositoryAdapterTest {

    @Autowired
    private PostRepositoryAdapter postRepositoryAdapter;

    @MockBean
    private PostJpaRepository postJpaRepository;

    private Post postMock;

    @BeforeEach
    public void setUp() {
        postMock = PostMock.getPost();
    }

    @Test
    public void Find_posts_by_page_return_ok() {
        //Given
        Pageable page = PageRequest.of(0, 10);
        Mockito.when(postJpaRepository.findAll(page)).thenReturn(new PageImpl<>(List.of(postMock)));
        //When
        Page<Post> postPage = postRepositoryAdapter.findPosts(page);
        //Then
        assertEquals(postMock, postPage.getContent().get(0));
        assertEquals(1, postPage.getSize());
        assertEquals(1, postPage.getTotalPages());
        Mockito.verify(this.postJpaRepository, Mockito.times(1)).findAll(page);
    }

    @Test
    public void Find_posts_by_id_return_ok() {
        //Given
        Long anyPostId = 1L;
        Mockito.when(postJpaRepository.findById(eq(anyPostId))).thenReturn(Optional.of(postMock));
        //When
        Post postResult = postRepositoryAdapter.findPostById(anyPostId);
        //Then
        assertEquals(postMock, postResult);
        Mockito.verify(this.postJpaRepository, Mockito.times(1)).findById(eq(anyPostId));
    }

    @Test
    public void Find_posts_by_id_throws_post_not_found() {
        //Given
        Long anyPostId = 1L;
        Mockito.when(postJpaRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        //Then
        ResourceNotFoundException ex = assertThrows(ResourceNotFoundException.class,
                () -> postRepositoryAdapter.findPostById(eq(anyPostId)));

        assertEquals("Post id not found", ex.getMessage());
        Mockito.verify(this.postJpaRepository, Mockito.times(1)).findById(Mockito.anyLong());
    }

    @Test
    public void Find_posts_by_title_return_ok() {
        //Given
        String anyTitle = "Some title";
        Mockito.when(postJpaRepository.findPostsByTitleContains(eq(anyTitle))).thenReturn(List.of(postMock));
        //When
        List<Post> postList = postRepositoryAdapter.findPostsByTitle(anyTitle);
        //Then
        assertEquals(postMock.getId(), postList.get(0).getId());
        assertEquals(postMock.getTitle(), postList.get(0).getTitle());
        Mockito.verify(this.postJpaRepository, Mockito.times(1))
                .findPostsByTitleContains(eq(anyTitle));
    }

    @Test
    public void Save_posts_return_ok() {
        //Given
        List<Post> postListMock = List.of(PostMock.getPost(1L), PostMock.getPost(2L));
        Mockito.when(postJpaRepository.saveAll(eq(postListMock))).thenReturn(postListMock);
        //When
        List<Post> postListResult = postRepositoryAdapter.saveAll(postListMock);
        //Then
        assertEquals(postMock.getId(), postListResult.get(0).getId());
        assertEquals(postMock.getTitle(), postListResult.get(0).getTitle());
        Mockito.verify(this.postJpaRepository, Mockito.times(1)).saveAll(eq(postListMock));
    }

}
