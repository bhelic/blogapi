package com.astro.blog.infrastructure.delivery.rest;

import com.astro.blog.domain.usecase.CommentUseCase;
import com.astro.blog.domain.usecase.PostUseCase;
import com.astro.blog.domain.usecase.dto.PostResponseDto;
import mocks.PostMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class PostControllerTest {

    @InjectMocks
    private PostController postController;

    @Mock
    private PostUseCase postUseCase;

    @Mock
    private CommentUseCase commentUseCase;

    @Test
    public void Get_post_by_id_return_ok() {
        //Given
        Long anyPostId = 1L;
        PostResponseDto postResponseMock = PostMock.getPostResponseDto();
        Mockito.when(postUseCase.getPostById(eq(anyPostId))).thenReturn(postResponseMock);
        //When
        ResponseEntity<PostResponseDto> postResult = postController.getPostById(anyPostId);
        //Then
        assertEquals(HttpStatus.OK, postResult.getStatusCode());
        assertNotNull(postResult.getBody());
        assertEquals(postResponseMock.getId(), postResult.getBody().getId());
        assertEquals(postResponseMock.getUserId(), postResult.getBody().getUserId());
        assertEquals(postResponseMock.getTitle(), postResult.getBody().getTitle());
        assertEquals(postResponseMock.getBody(), postResult.getBody().getBody());
    }

}