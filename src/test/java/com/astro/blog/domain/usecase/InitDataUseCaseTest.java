package com.astro.blog.domain.usecase;

import com.astro.blog.domain.model.Comment;
import com.astro.blog.domain.model.Post;
import com.astro.blog.domain.model.gateway.CommentRepository;
import com.astro.blog.domain.model.gateway.PostRepository;
import com.astro.blog.domain.model.gateway.port.CommentPort;
import com.astro.blog.domain.model.gateway.port.PostPort;
import com.astro.blog.domain.usecase.dto.CommentResponseDto;
import com.astro.blog.domain.usecase.dto.PostResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import mocks.CommentMock;
import mocks.PostMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class InitDataUseCaseTest {

    @InjectMocks
    private InitDataUseCase initDataUseCase;
    @Mock
    private PostPort postPort;
    @Mock
    private CommentPort commentPort;
    @Mock
    private PostRepository postRepository;
    @Mock
    private CommentRepository commentRepository;
    @Spy
    private ObjectMapper objectMapper;

    @Test
    public void Init_data_return_ok() {
        //Given
        PostResponseDto[] postResponseArray = {
                PostMock.getPostResponseDto(1L),
                PostMock.getPostResponseDto(2L)
        };
        Mockito.when(postPort.getPosts()).thenReturn(postResponseArray);

        List<Post> postList = List.of(
                PostMock.getPost(1L),
                PostMock.getPost(2L)
        );
        Mockito.when(postRepository.saveAll(Mockito.any())).thenReturn(postList);

        CommentResponseDto commentResponseMock = CommentMock.getCommentResponseDto();
        Mockito.when(commentPort.getComments(Mockito.anyLong())).thenReturn(new CommentResponseDto[]{commentResponseMock});

        Comment commentMock = CommentMock.getComment();
        Mockito.when(commentRepository.saveAll(Mockito.any())).thenReturn(List.of(commentMock));
        //When
        List<Post> posts = this.initDataUseCase.initData();
        //Then
        assertEquals(postList.get(0).getId(), posts.get(0).getId());
        assertEquals(postList.get(0).getUserId(), posts.get(0).getUserId());
        assertEquals(postList.get(0).getTitle(), posts.get(0).getTitle());
        assertEquals(postList.get(0).getBody(), posts.get(0).getBody());
        Mockito.verify(this.postPort, Mockito.times(1)).getPosts();
        Mockito.verify(this.postRepository, Mockito.times(1)).saveAll(Mockito.any());
        Mockito.verify(this.commentPort, Mockito.times(2)).getComments(Mockito.anyLong());
        Mockito.verify(this.commentRepository, Mockito.times(2)).saveAll(Mockito.any());
    }

}
