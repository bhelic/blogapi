package com.astro.blog.domain.usecase;

import com.astro.blog.application.configuration.errors.ResourceNotFoundException;
import com.astro.blog.domain.model.Post;
import com.astro.blog.domain.model.gateway.PostRepository;
import com.astro.blog.domain.usecase.dto.PostResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import mocks.PostMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class PostUseCaseTest {

    @InjectMocks
    private PostUseCase postUseCase;
    @Mock
    private PostRepository postRepository;
    @Spy
    private ObjectMapper objectMapper;

    private Post postMock;

    @BeforeEach
    public void setUp() {
        postMock = PostMock.getPost();
    }

    @Test
    public void Get_posts_by_page_return_ok() {
        //Given
        Pageable somePage = PageRequest.of(0, 10);
        Mockito.when(postRepository.findPosts(somePage)).thenReturn(new PageImpl<>(List.of(postMock)));
        //When
        Page<PostResponseDto> page = this.postUseCase.getPosts(somePage);
        //Then
        assertEquals(1, page.getSize());
        assertEquals(1, page.getTotalPages());
        assertEquals(this.postMock.getId(), page.getContent().get(0).getId());
        assertEquals(this.postMock.getUserId(), page.getContent().get(0).getUserId());
        assertEquals(this.postMock.getTitle(), page.getContent().get(0).getTitle());
        assertEquals(this.postMock.getBody(), page.getContent().get(0).getBody());
        Mockito.verify(this.postRepository, Mockito.times(1)).findPosts(somePage);
    }

    @Test
    public void Get_post_by_title_return_ok() {
        //Given
        String someTitle = "Some title";
        Mockito.when(postRepository.findPostsByTitle(eq(someTitle))).thenReturn(List.of(postMock));
        //When
        List<PostResponseDto> posts = this.postUseCase.getPostsByTitle(someTitle);
        //Then
        assertEquals(this.postMock.getId(), posts.get(0).getId());
        assertEquals(this.postMock.getUserId(), posts.get(0).getUserId());
        assertEquals(this.postMock.getTitle(), posts.get(0).getTitle());
        assertEquals(this.postMock.getBody(), posts.get(0).getBody());
        Mockito.verify(this.postRepository, Mockito.times(1)).findPostsByTitle(someTitle);
    }

    @Test
    public void Get_post_by_id_return_ok() {
        //Given
        Mockito.when(postRepository.findPostById(1L)).thenReturn(postMock);
        //When
        PostResponseDto post = this.postUseCase.getPostById(1L);
        //Then
        assertEquals(this.postMock.getId(), post.getId());
        assertEquals(this.postMock.getUserId(), post.getUserId());
        assertEquals(this.postMock.getTitle(), post.getTitle());
        assertEquals(this.postMock.getBody(), post.getBody());
        Mockito.verify(this.postRepository, Mockito.times(1)).findPostById(1L);
    }

    @Test
    public void Get_post_by_id_throws_post_not_found() {
        //Given
        Mockito.when(postRepository.findPostById(2L)).thenThrow(new ResourceNotFoundException("Post id not found"));
        //Then
        ResourceNotFoundException ex = assertThrows(ResourceNotFoundException.class,
                () -> this.postUseCase.getPostById(2L));

        assertEquals("Post id not found", ex.getMessage());
        Mockito.verify(this.postRepository, Mockito.times(1)).findPostById(2L);
    }

}
