package com.astro.blog.domain.usecase;

import com.astro.blog.domain.model.Comment;
import com.astro.blog.domain.model.gateway.CommentRepository;
import com.astro.blog.domain.usecase.dto.CommentResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import mocks.CommentMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CommentUseCaseTest {

    @InjectMocks
    private CommentUseCase commentUseCase;
    @Mock
    private CommentRepository commentRepository;
    @Spy
    private ObjectMapper objectMapper;

    @Test
    public void Get_comments_by_post_id_return_ok() {
        //Given
        Comment commentMock = CommentMock.getComment();
        Mockito.when(commentRepository.findCommentsByPostId(Mockito.anyLong())).thenReturn(List.of(commentMock));
        //When
        List<CommentResponseDto> comments = this.commentUseCase.getCommentsByPostId(1L);
        //Then
        assertEquals(commentMock.getId(), comments.get(0).getId());
        assertEquals(commentMock.getBody(), comments.get(0).getBody());
        assertEquals(commentMock.getEmail(), comments.get(0).getEmail());
        assertEquals(commentMock.getName(), comments.get(0).getName());
        assertEquals(commentMock.getPost().getId(), comments.get(0).getPostId());
        Mockito.verify(this.commentRepository, Mockito.times(1)).findCommentsByPostId(1L);
    }

}
