package mocks;

import com.astro.blog.domain.model.Comment;
import com.astro.blog.domain.usecase.dto.CommentResponseDto;

public class CommentMock {

    public static Comment getComment() {
        Comment comment = new Comment();
        comment.setId(100L);
        comment.setName("Name");
        comment.setEmail("Email");
        comment.setBody("Comment body");
        comment.setPost(PostMock.getPost());
        return comment;
    }

    public static CommentResponseDto getCommentResponseDto() {
        CommentResponseDto commentResponseDto = new CommentResponseDto();
        commentResponseDto.setId(100L);
        commentResponseDto.setName("Name");
        commentResponseDto.setEmail("Email");
        commentResponseDto.setBody("Comment body");
        commentResponseDto.setPostId(1L);
        return commentResponseDto;
    }

}
