package mocks;

import com.astro.blog.domain.model.Post;
import com.astro.blog.domain.usecase.dto.PostResponseDto;

public class PostMock {

    public static Post getPost() {
        Post post = new Post();
        post.setId(1L);
        post.setUserId(10L);
        post.setTitle("Some title");
        post.setBody("Post body");
        return post;
    }

    public static Post getPost(Long postId) {
        Post post = getPost();
        post.setId(postId);
        return post;
    }

    public static PostResponseDto getPostResponseDto() {
        PostResponseDto postResponseDto = new PostResponseDto();
        postResponseDto.setId(1L);
        postResponseDto.setUserId(10L);
        postResponseDto.setTitle("Some title");
        postResponseDto.setBody("Post body");
        return postResponseDto;
    }

    public static PostResponseDto getPostResponseDto(Long postId) {
        PostResponseDto postResponseDto = getPostResponseDto();
        postResponseDto.setId(postId);
        return postResponseDto;
    }

}
